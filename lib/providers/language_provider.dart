import 'package:bilalqwatly_test/utilities/app_constants.dart';
import 'package:bilalqwatly_test/utilities/app_shared%20_preference.dart';
import 'package:flutter/material.dart';

class LanguageProvider with ChangeNotifier {
  late Locale _appLocale;

  LanguageProvider() {


    initLocale();
  }

  set appLocale(Locale value) {
    _appLocale = value;
    notifyListeners();
  }

  Locale get appLocale => _appLocale;

  void initLocale() async {
    // if language set to arabic make app language arabic otherwise make it english
    // default language is english

    if (AppSharedPreference.getLanguagePrefs() ==
        AppConstants.arabicConst) {
      changeLanguage(AppConstants.arabicConst);
    } else {
      changeLanguage(AppConstants.englishConst);
    }

    notifyListeners();
  }

  Future<void> changeLanguage(String language) async {

    if (language == AppConstants.arabicConst) {
      appLocale = const Locale(AppConstants.arabicConst);

      AppSharedPreference.setLanguagePrefs(AppConstants.arabicConst);

    } else if (language == AppConstants.englishConst) {
      appLocale = const Locale(AppConstants.englishConst);
      AppSharedPreference.setLanguagePrefs(AppConstants.englishConst);


    }

  }
}
