import 'package:bilalqwatly_test/models/general_api_models/meta_model.dart';
import 'package:bilalqwatly_test/models/response/me_response_model.dart';
import 'package:bilalqwatly_test/models/response/passion_response_model.dart';
import 'package:bilalqwatly_test/network/rest_client.dart';
import 'package:bilalqwatly_test/utilities/app_shared%20_preference.dart';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart' hide Headers;

class AppProvider with ChangeNotifier {
  late RestClient restRequest;

  bool isConnected = false;
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  Future<bool> checkInternetConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    isConnected = connectivityResult != ConnectivityResult.none;
    return isConnected;
  }

  late bool _responseStatus;

  bool get responseStatus => _responseStatus;

  set responseStatus(bool value) {
    _responseStatus = value;
    notifyListeners();
  }

  String _errorResponseMessage = "";

  String get errorResponseMessage => _errorResponseMessage;

  set errorResponseMessage(String value) {
    _errorResponseMessage = value;
    notifyListeners();
  }

  Future<PassionResponseModel> getPassion(BuildContext context) async {
    late PassionResponseModel passionResponseModel;

    try {
      restRequest = RestClient(Dio(), context: context);

      isLoading = true;
      passionResponseModel = await restRequest.getPassion();
      for (int i = 0; i < passionResponseModel.interestsList!.length; i++) {
        passionResponseModel.interestsList!.elementAt(i).isSelected = false;
      }
    } catch (e) {
      isLoading = false;
    }
    isLoading = false;

    return passionResponseModel;
  }

  Future<MeResponseModel> getMeProfile(BuildContext context) async {
    late MeResponseModel meResponseModel;

    try {
      restRequest = RestClient(Dio(), context: context);

      isLoading = true;
      meResponseModel = await restRequest.getMeProfile();

      for (int i = 0; i < meResponseModel.profile!.interestsList!.length; i++) {
        meResponseModel.profile!.interestsList!.elementAt(i).isSelected = false;
      }

    } catch (e) {
      isLoading = false;

      print("Bilaloasfsafsafs" + e.toString());
    }
    isLoading = false;

    return meResponseModel;
  }

}
