import 'package:bilalqwatly_test/main.dart';
import 'package:bilalqwatly_test/models/response/photo_response_model.dart';
import 'package:bilalqwatly_test/utilities/app_images.dart';
import 'package:flutter/material.dart';

class ListItemPhoto extends StatefulWidget {

  final PhotoResponseModel photoResponseModel;

  const ListItemPhoto({Key? key, required this.photoResponseModel}) : super(key: key);

  @override
  _ListItemPhotoState createState() => _ListItemPhotoState();
}

class _ListItemPhotoState extends State<ListItemPhoto> {
  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
            image: AppImages.getCachedNetworkImageProvider(
                widget.photoResponseModel.filename!,
            )),
      ),
    );
  }
}
