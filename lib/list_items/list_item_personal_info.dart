import 'package:bilalqwatly_test/main.dart';
import 'package:bilalqwatly_test/models/response/basic_info_response_model.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_style_text.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:flutter/material.dart';

class ListItemPersonalInfo extends StatefulWidget {
  final BasicInfoResponseModel basicInfoResponseModel;
  final bool islast;

  const ListItemPersonalInfo(
      {Key? key, required this.basicInfoResponseModel, required this.islast})
      : super(key: key);

  @override
  _ListItemPersonalInfoState createState() => _ListItemPersonalInfoState();
}

class _ListItemPersonalInfoState extends State<ListItemPersonalInfo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        /// Name
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Text(widget.basicInfoResponseModel.key!.name!,
                  style: AppStyleText.helveticaNeuLight17WhiteFFFFFF()),
            ),
            Expanded(
                flex: 1,
                child: Text(
                  widget.basicInfoResponseModel.value!,
                  style: AppStyleText.helveticaNeuMedium16WhiteFFFFFF(),
                )),
          ],
        ),
        SizedBox(
          height: AppSuitableWidgetSize.getSuitableWidgetHeight(15),
        ),
        Container(
          height: widget.islast
              ? AppSuitableWidgetSize.getSuitableWidgetHeight(2)
              : 0,
          width: appWidthScreen,
          color: AppColors.blue0A2175_37,
        ),
        SizedBox(
          height: AppSuitableWidgetSize.getSuitableWidgetHeight(15),
        ),
      ],
    );
  }
}
