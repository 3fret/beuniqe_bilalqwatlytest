import 'package:bilalqwatly_test/models/general_api_models/meta_model.dart';
import 'package:bilalqwatly_test/utilities/app_localizations.dart';
import 'package:bilalqwatly_test/utilities/utility.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:flutter/cupertino.dart';

class HandelHttpError {
  static MetaModel handleHttpError(DioError dioError, BuildContext context) {
    var response = dioError.response?.data;
    late MetaModel meta;

    if (response != null &&
        dioError.response?.statusCode != 500 &&
        dioError.response?.statusCode != 404) meta = response["meta"] ?? "";
    var statusCode = dioError.response?.statusCode;

    switch (dioError.type) {
      case DioErrorType.cancel:
        meta = MetaModel(
            message: AppLocalizations.of(context)!
                .translate('request_was_cancelled'),
            status: false,
            picturePath: "",
            statusCode: 0);
        break;

      case DioErrorType.connectTimeout:
        meta = MetaModel(
            message:
                AppLocalizations.of(context)!.translate('connection_timeout'),
            status: false,
            picturePath: "",
            statusCode: 0);
        break;

      case DioErrorType.other:
        meta = MetaModel(
            message: AppLocalizations.of(context)!.translate('other_error'),
            status: false,
            picturePath: "",
            statusCode: 0);
        break;

      case DioErrorType.receiveTimeout:
        meta = MetaModel(
            message: AppLocalizations.of(context)!
                .translate('receive_timeout_in_connection'),
            status: false,
            picturePath: "",
            statusCode: 0);
        break;

      case DioErrorType.response:
        switch (statusCode) {


          case 404:
            meta = MetaModel(
                message:
                    AppLocalizations.of(context)!.translate('url_not_found'),
                status: false,
                picturePath: '',
                statusCode: 404);
            break;

          case 405:
            meta = MetaModel(
                message: AppLocalizations.of(context)!
                    .translate('method_not_allowed'),
                status: false,
                picturePath: '',
                statusCode: 405);
            break;

          case 500:
            meta = MetaModel(
                message:
                    AppLocalizations.of(context)!.translate('server_error'),
                status: false,
                picturePath: '',
                statusCode: 500);
            break;
        }
        break;

      case DioErrorType.sendTimeout:
        meta = MetaModel(
            message: AppLocalizations.of(context)!
                .translate('receive_timeout_in_send_request'),
            status: false,
            picturePath: "",
            statusCode: 0);
        break;
    }

    if (statusCode == null) {
      Utility.showToast(context,
          AppLocalizations.of(context)!.translate('no_internet_connection'));
      meta = MetaModel(
          message:
              AppLocalizations.of(context)!.translate('no_internet_connection'),
          status: false,
          picturePath: "",
          statusCode: 0);
    }

    return meta;
  }
}
