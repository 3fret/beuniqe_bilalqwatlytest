import 'dart:convert';
import 'package:bilalqwatly_test/models/response/me_response_model.dart';
import 'package:bilalqwatly_test/models/response/passion_response_model.dart';
import 'package:bilalqwatly_test/providers/app_provider.dart';
import 'package:bilalqwatly_test/utilities/app_constants.dart';
import 'package:bilalqwatly_test/utilities/app_shared%20_preference.dart';
import 'package:bilalqwatly_test/utilities/utility.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:provider/provider.dart';
import 'package:retrofit/http.dart';
import 'handle_http_error.dart';


part 'rest_client.g.dart';

@RestApi(baseUrl: 'https://api.zipconnect.app/')
abstract class RestClient {
  factory RestClient(Dio dio, {BuildContext? context, String? baseUrl}) {
    // /// for ignore ssl certificate
    // (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
    //     (HttpClient client) {
    //   client.badCertificateCallback =
    //       (X509Certificate cert, String host, int port) => true;
    //   return client;
    // };

    //
    // CacheConfig cacheConfig = CacheConfig();
    // DioCacheManager _dioCacheManager = DioCacheManager(cacheConfig);
    //
    //
    //
    //
    // dio.interceptors.add(_dioCacheManager.interceptor);
    dio.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      // Do something before request is sent

      options.connectTimeout = 60000;
      options.receiveTimeout = 60000;
      var customHeaders = {
        'content-type': 'application/json',
        "Accept-Language":
            AppSharedPreference.getLanguagePrefs() ?? AppConstants.englishConst,
         'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZDQ4ODBmMzdhM2UxNTk1MzNjN2E3NyIsImlhdCI6MTY0MTMxODQyNiwiZXhwIjoxNjQ5MDk0NDI2fQ.oJ0ykp8Hh2tyTBKie7XTVf05OwaxtQsx4vqRNIGkgyI"


        // other headers
      };

      options.headers.addAll(customHeaders);
      return handler.next(options); //continue
      // If you want to resolve the request with some custom data，
      // you can resolve a `Response` object eg: return `dio.resolve(response)`.
      // If you want to reject the request with a error message,
      // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    }, onResponse: (response, handler) {
      // Do something with response data

      try {
        var status = jsonDecode(response.toString())["status"];

        if (status == AppConstants.errorApiValue) {
          Provider.of<AppProvider>(context!, listen: false).responseStatus =
              false;

          if (jsonDecode(response.toString())["message"] != null) {
            if (!status) {
              Utility.showToast(
                  context, jsonDecode(response.toString())["message"]);
            }
          }
        } else {
          Provider.of<AppProvider>(context!, listen: false).responseStatus =
              true;
        }

        Map<String, dynamic> data = jsonDecode(response.toString())["data"];
        response.data = data;
      } catch (e) {
        if (kDebugMode) {
          print('error in parse ${e.toString()}');
        }
      }

      return handler.next(response); // continue

      // If you want to reject the request with a error message,
      // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    }, onError: (DioError e, handler) async {
      Provider.of<AppProvider>(context!, listen: false).responseStatus =
          HandelHttpError.handleHttpError(e, context).status!;
      Provider.of<AppProvider>(context, listen: false).errorResponseMessage =
      HandelHttpError.handleHttpError(e, context).message!;
      return handler.reject(e);
      // If you want to resolve the request with some custom data，
      // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    }));

    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        request: true,
        error: true,
        compact: true,
        maxWidth: 90));

    return _RestClient(dio, baseUrl: baseUrl);
  }

  @GET("api/v1/category/616bdaa6a1b49050b2723b31")
  Future<PassionResponseModel> getPassion();

  @GET("api/v1/profile/me")
  Future<MeResponseModel> getMeProfile();

}
