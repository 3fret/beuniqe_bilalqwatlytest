
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context)
  {
    return Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Container(
              padding: EdgeInsetsDirectional.all(
                AppSuitableWidgetSize.getSuitableWidgetHeight(8)
              ),
              child: SizedBox(
                height: AppSuitableWidgetSize.getSuitableWidgetHeight(40) ,
                width: AppSuitableWidgetSize.getSuitableWidgetWidth(40),
                child: CircularProgressIndicator(
                  strokeWidth: AppSuitableWidgetSize.getSuitableWidgetWidth(4),
                  valueColor: const AlwaysStoppedAnimation(AppColors.whiteFFFFFF),
                ),
              ),
            ),
          ],
        ));
  }
}
