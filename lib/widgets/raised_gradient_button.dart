import 'package:bilalqwatly_test/main.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RaisedGradientButton extends StatelessWidget {
  final Widget child;
  final Gradient gradient;

  final VoidCallback onPressed;

  const RaisedGradientButton({
    Key? key,
    required this.child,
    required this.gradient,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: appWidthScreen * 0.8 ,
      height:AppSuitableWidgetSize.getSuitableWidgetHeight(53),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(
              AppSuitableWidgetSize.getSuitableWidgetHeight(8))),
          gradient: gradient,
          boxShadow: const [
            BoxShadow(
              offset: Offset(0.0,0.5),
              blurRadius: 0.5,
            ),
          ]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: onPressed,
            child: Center(
              child: child,
            )),
      ),
    );
  }
}
