
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_localizations.dart';
import 'package:bilalqwatly_test/utilities/app_style_text.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class EmptyPage extends StatelessWidget {
  const EmptyPage({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width:
                  AppSuitableWidgetSize.getSuitableWidgetWidth(100),
              height:
                  AppSuitableWidgetSize.getSuitableWidgetHeight(100),
              child: SvgPicture.asset('assets/images/empty.svg',
                height: AppSuitableWidgetSize.getSuitableWidgetHeight(150),
                width: AppSuitableWidgetSize.getSuitableWidgetWidth(150),
                color: AppColors.black070014,),
            ),
            SizedBox(
              height:
                  AppSuitableWidgetSize.getSuitableWidgetHeight(20),
            ),
            RichText(
              text: TextSpan(
                text: AppLocalizations.of(context)!.translate('no_items_found'),
                style: AppStyleText.arialRegular14WhiteFFFFFF(),
              ),
            ),
            SizedBox(
              height:
                  AppSuitableWidgetSize.getSuitableWidgetHeight(20),
            ),
          ],
        ),
      ),
    );
  }
}
