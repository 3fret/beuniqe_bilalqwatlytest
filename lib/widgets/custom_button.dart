import 'package:bilalqwatly_test/utilities/app_localizations.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final String text;
  final double width;
  final double height;
  final double radius;
  final double borderWidth;
  final Color color;
  final Color borderColor;
  final TextStyle textStyle;
  final VoidCallback onPressed;

  const CustomButton({
    Key? key,
    required this.text,
    required this.width,
    required this.height,
    required this.color,
    required this.borderColor,
    required this.borderWidth,
    required this.radius,
    required this.textStyle,
    required this.onPressed,
  }) : super(key: key);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * widget.width,
      height: widget.height,
      child: ElevatedButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(widget.color),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              side: BorderSide(
                  color: widget.borderColor,
                  width: widget.borderWidth,
                  style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(widget.radius),
            ))),
        child: Text(
          widget.text,
          style: widget.textStyle,
        ),
        onPressed: () {
          widget.onPressed.call();
        },
      ),
    );
  }
}
