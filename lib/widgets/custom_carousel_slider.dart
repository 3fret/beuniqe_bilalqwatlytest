import 'package:bilalqwatly_test/list_items/list_item_photo.dart';
import 'package:bilalqwatly_test/main.dart';
import 'package:bilalqwatly_test/models/response/photo_response_model.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CustomCarouselSlider extends StatefulWidget {
  final List<PhotoResponseModel> photoList;

  const CustomCarouselSlider({Key? key, required this.photoList})
      : super(key: key);

  @override
  _CustomCarouselSliderState createState() => _CustomCarouselSliderState();
}

class _CustomCarouselSliderState extends State<CustomCarouselSlider> {
  int _current = 0;
  final CarouselController _controller = CarouselController();
  List<ListItemPhoto> photoListItemLists = [];

  @override
  Widget build(BuildContext context) {
    if (photoListItemLists.isEmpty) {
      for (var photoModel in widget.photoList) {
        photoListItemLists.add(ListItemPhoto(photoResponseModel: photoModel));
      }
    }

    return Stack(children: [
      CarouselSlider(
        options: CarouselOptions(
            autoPlay: true,
            scrollPhysics: const BouncingScrollPhysics(),
            aspectRatio: 0.7,
            viewportFraction: 1,
            autoPlayInterval: const Duration(seconds: 6),
            onPageChanged: (index, reason) {
              setState(() {
                _current = index;
              });
            }),
        items: widget.photoList.map((photoModel) {
          return Builder(
            builder: (BuildContext context) {
              return ListItemPhoto(
                photoResponseModel: photoModel,
              );
            },
          );
        }).toList(),
      ),
      /// Slider Indicator
      Positioned(
        bottom: 35,
        left: 0,
        right: 0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: photoListItemLists.asMap().entries.map((entry) {
            return GestureDetector(
              onTap: () => _controller.animateToPage(entry.key),
              child: Container(
                width: AppSuitableWidgetSize.getSuitableWidgetWidth(10),
                height: AppSuitableWidgetSize.getSuitableWidgetHeight(10),
                margin:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == entry.key
                        ? AppColors.whiteFFFFFF
                        :  AppColors.whiteFFFFFF.withOpacity(0.3)),
              ),
            );
          }).toList(),
        ),
      ),
    ]);
  }
}
