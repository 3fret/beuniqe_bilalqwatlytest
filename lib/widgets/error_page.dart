import 'package:bilalqwatly_test/providers/app_provider.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_localizations.dart';
import 'package:bilalqwatly_test/utilities/app_style_text.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:bilalqwatly_test/widgets/raised_gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class ErrorPage extends StatefulWidget {
  final VoidCallback? onPressed;

  const ErrorPage({Key? key, required this.onPressed}) : super(key: key);

  @override
  State<ErrorPage> createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsetsDirectional.all(AppSuitableWidgetSize.getSuitableWidgetWidth(15)),
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width:
                  AppSuitableWidgetSize.getSuitableWidgetWidth(100),
              height:
                  AppSuitableWidgetSize.getSuitableWidgetHeight(100),
              child: SvgPicture.asset('assets/images/error.svg',
                height: AppSuitableWidgetSize.getSuitableWidgetHeight(150),
                width: AppSuitableWidgetSize.getSuitableWidgetWidth(150),

                color: AppColors.whiteFFFFFF,),
            ),
            SizedBox(
              height:
                  AppSuitableWidgetSize.getSuitableWidgetHeight(20),
            ),
            RichText(
              text: TextSpan(
                text:
                    '',
                style: AppStyleText.arialRegular14WhiteFFFFFF(),
                children: <TextSpan>[
                  TextSpan(
                      text:
                        Provider.of<AppProvider>(context).errorResponseMessage,
                      style: AppStyleText.arialRegular14WhiteFFFFFF()),
                ],
              ),
            ),
            SizedBox(
              height:
                  AppSuitableWidgetSize.getSuitableWidgetHeight(20),
            ),
            RaisedGradientButton(
              child: Text(
                AppLocalizations.of(context)!.translate("try_again"),
                style: AppStyleText.helveticaNeuBold14WhiteFFFFFF(),
              ),
              gradient: const LinearGradient(
                colors: <Color>[AppColors.blue4B6FFF, AppColors.blue0226B2],
              ),
              onPressed: () {
                widget.onPressed!.call();
              },
            ),

          ],
        ),
      ),
    );
  }
}
