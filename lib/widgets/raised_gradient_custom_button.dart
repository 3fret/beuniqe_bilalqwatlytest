import 'package:bilalqwatly_test/main.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_style_text.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RaisedGradientCustomButton extends StatelessWidget {
  final Color color;
  final String text;

  const RaisedGradientCustomButton({
    Key? key,
    required this.color,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: appWidthScreen * 0.8 ,
      height:AppSuitableWidgetSize.getSuitableWidgetHeight(53),
      decoration: BoxDecoration(
        color: color,
          borderRadius: BorderRadius.all(Radius.circular(
              AppSuitableWidgetSize.getSuitableWidgetHeight(7))),
          ),
      child: Center(
        child: Text(text,style: AppStyleText.helveticaNeuBold13WhiteFFFFFF(),),
      ),
    );
  }
}
