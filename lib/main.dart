import 'package:bilalqwatly_test/providers/language_provider.dart';
import 'package:bilalqwatly_test/screens/bio_paid_screen.dart';
import 'package:bilalqwatly_test/screens/passion.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'providers/app_provider.dart';
import 'utilities/app_constants.dart';
import 'utilities/app_localizations.dart';

late SharedPreferences prefs;
late Widget firstWidget;
late BuildContext appContext;
late double appWidthScreen,appHeightScreen,appAspectRatio;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();


  /// init SharedPreference
  prefs = await SharedPreferences.getInstance();

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (BuildContext context) => AppProvider(),
      ),
      ChangeNotifierProvider(
        create: (BuildContext context) => LanguageProvider(),
      ),
    ],
    child: Consumer<LanguageProvider>(
      builder: (context, languageProvider, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          debugShowMaterialGrid: false,


          /// begin language issue
          locale: languageProvider.appLocale,
          supportedLocales: const [
            Locale(AppConstants.englishConst),
          ],
          localizationsDelegates: const [
            AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],

          /// end language issue

          /// begin theme issue
          theme: ThemeData(
            scrollbarTheme:  ScrollbarThemeData(
              isAlwaysShown: true,
              mainAxisMargin: 50,
              crossAxisMargin:  20,
              trackColor: MaterialStateProperty.all(AppColors.whiteFFFFFF),
              thickness: MaterialStateProperty.all(1),
              thumbColor: MaterialStateProperty.all(AppColors.whiteFFFFFF),
              interactive: false,
              showTrackOnHover: true,
              trackBorderColor: MaterialStateProperty.all(AppColors.whiteFFFFFF),
              minThumbLength: 100),
            scaffoldBackgroundColor: AppColors.black070014,
            fontFamily: languageProvider.appLocale.languageCode ==
                    AppConstants.englishConst
                ? AppConstants.englishFont
                : AppConstants.arabicFont,
            brightness: Brightness.dark,
            hoverColor: AppColors.whiteFFFFFF,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),

          /// end theme issue

          home: const MyApp(),
          // routes: {
          //   '/': (context) => MyApp(),
          //   'notification': (context) => NotificationPage(),
          //   'main_page': (context) => MainPage(),
          //   'login_page.dart': (context) => LoginPage(),
          // },
        );
      },
    ),
  ));
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: AppColors.black070014,
      systemNavigationBarDividerColor: AppColors.black070014,
      statusBarBrightness: Brightness.light,
      systemNavigationBarColor: AppColors.black070014,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.light));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    appWidthScreen = MediaQuery.of(context).size.width;
    appHeightScreen = MediaQuery.of(context).size.height;
    appAspectRatio = MediaQuery.of(context).size.aspectRatio;
    appContext = context;


    return const PassionScreen();
  }
}
