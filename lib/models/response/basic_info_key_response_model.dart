import 'package:json_annotation/json_annotation.dart';

part 'basic_info_key_response_model.g.dart';

@JsonSerializable()
class BasicInfoKeyResponseModel {

  @JsonKey(name: 'name')
  String? name;


  BasicInfoKeyResponseModel({
    required this.name,
  });

  factory BasicInfoKeyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$BasicInfoKeyResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$BasicInfoKeyResponseModelToJson(this);
}
