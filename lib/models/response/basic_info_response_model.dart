import 'package:json_annotation/json_annotation.dart';

import 'basic_info_key_response_model.dart';

part 'basic_info_response_model.g.dart';

@JsonSerializable()
class BasicInfoResponseModel {

  @JsonKey(name: 'value')
  String? value;


  @JsonKey(name: 'key')
  BasicInfoKeyResponseModel? key;

  BasicInfoResponseModel({
    required this.value,
    required this.key,
  });

  factory BasicInfoResponseModel.fromJson(Map<String, dynamic> json) =>
      _$BasicInfoResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$BasicInfoResponseModelToJson(this);
}
