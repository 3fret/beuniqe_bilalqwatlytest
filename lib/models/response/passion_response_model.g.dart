// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'passion_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PassionResponseModel _$PassionResponseModelFromJson(
        Map<String, dynamic> json) =>
    PassionResponseModel(
      id: json['_id'] as String?,
      name: json['name'] as String?,
      premium: json['premium'] as bool?,
      selection: json['selection'] as String?,
      category: json['category'] as String?,
      slug: json['slug'] as String?,
      interestsList: (json['interests'] as List<dynamic>?)
          ?.map(
              (e) => InterestsResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PassionResponseModelToJson(
        PassionResponseModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'premium': instance.premium,
      'name': instance.name,
      'selection': instance.selection,
      'category': instance.category,
      'slug': instance.slug,
      'interests': instance.interestsList,
    };
