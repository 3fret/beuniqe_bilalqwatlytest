// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileResponseModel _$ProfileResponseModelFromJson(
        Map<String, dynamic> json) =>
    ProfileResponseModel(
      photos: (json['photos'] as List<dynamic>?)
          ?.map((e) => PhotoResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      name: json['name'] as String?,
      age: json['age'] as int?,
      gender: json['gender'] as String?,
      location: json['location'] == null
          ? null
          : LocationResponseModel.fromJson(
              json['location'] as Map<String, dynamic>),
      basicInfoList: (json['basic_info'] as List<dynamic>?)
          ?.map(
              (e) => BasicInfoResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      interestsList: (json['interests'] as List<dynamic>?)
          ?.map(
              (e) => InterestsResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ProfileResponseModelToJson(
        ProfileResponseModel instance) =>
    <String, dynamic>{
      'photos': instance.photos,
      'name': instance.name,
      'gender': instance.gender,
      'age': instance.age,
      'location': instance.location,
      'basic_info': instance.basicInfoList,
      'interests': instance.interestsList,
    };
