// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'interests_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InterestsResponseModel _$InterestsResponseModelFromJson(
        Map<String, dynamic> json) =>
    InterestsResponseModel(
      name: json['name'] as String?,
      image: json['image'] as String?,
      isSelected: json['isSelected'] as bool?,
    );

Map<String, dynamic> _$InterestsResponseModelToJson(
        InterestsResponseModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'image': instance.image,
      'isSelected': instance.isSelected,
    };
