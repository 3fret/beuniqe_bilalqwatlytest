import 'package:json_annotation/json_annotation.dart';

part 'photo_response_model.g.dart';

@JsonSerializable()
class PhotoResponseModel {
  @JsonKey(name: 'filename')
  String? filename;

  PhotoResponseModel({
    required this.filename,
  });

  factory PhotoResponseModel.fromJson(Map<String, dynamic> json) =>
      _$PhotoResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoResponseModelToJson(this);
}
