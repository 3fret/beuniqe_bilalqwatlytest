// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'me_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MeResponseModel _$MeResponseModelFromJson(Map<String, dynamic> json) =>
    MeResponseModel(
      profile: json['profile'] == null
          ? null
          : ProfileResponseModel.fromJson(
              json['profile'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MeResponseModelToJson(MeResponseModel instance) =>
    <String, dynamic>{
      'profile': instance.profile,
    };
