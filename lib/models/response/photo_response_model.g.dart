// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoResponseModel _$PhotoResponseModelFromJson(Map<String, dynamic> json) =>
    PhotoResponseModel(
      filename: json['filename'] as String?,
    );

Map<String, dynamic> _$PhotoResponseModelToJson(PhotoResponseModel instance) =>
    <String, dynamic>{
      'filename': instance.filename,
    };
