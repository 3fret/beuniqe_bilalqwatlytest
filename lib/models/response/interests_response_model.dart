import 'package:json_annotation/json_annotation.dart';

part 'interests_response_model.g.dart';

@JsonSerializable()
class InterestsResponseModel {




  @JsonKey(name: 'name')
  String? name;



  @JsonKey(name: 'image')
  String? image;

  bool? isSelected = false;
  InterestsResponseModel(
      {

      required this.name,
      required this.image,
      required this.isSelected
      });

  factory InterestsResponseModel.fromJson(Map<String, dynamic> json) =>
      _$InterestsResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$InterestsResponseModelToJson(this);
}
