import 'package:json_annotation/json_annotation.dart';
import 'interests_response_model.dart';

part 'passion_response_model.g.dart';

@JsonSerializable()
class PassionResponseModel {
  @JsonKey(name: '_id')
  String? id;

  @JsonKey(name: 'premium')
  bool? premium;

  @JsonKey(name: 'name')
  String? name;

  @JsonKey(name: 'selection')
  String? selection;

  @JsonKey(name: 'category')
  String? category;

  @JsonKey(name: 'slug')
  String? slug;

  @JsonKey(name: 'interests')
  List<InterestsResponseModel>? interestsList;



  PassionResponseModel(
      {required this.id,
      required this.name,
      required this.premium,
      required this.selection,
      required this.category,
      required this.slug,
      required this.interestsList});

  factory PassionResponseModel.fromJson(Map<String, dynamic> json) =>
      _$PassionResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$PassionResponseModelToJson(this);
}
