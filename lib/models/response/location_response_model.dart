import 'package:json_annotation/json_annotation.dart';

part 'location_response_model.g.dart';

@JsonSerializable()
class LocationResponseModel {
  @JsonKey(name: 'address')
  String? address;

  LocationResponseModel({
    required this.address,
  });

  factory LocationResponseModel.fromJson(Map<String, dynamic> json) =>
      _$LocationResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$LocationResponseModelToJson(this);
}
