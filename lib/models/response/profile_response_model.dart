import 'package:bilalqwatly_test/models/response/basic_info_response_model.dart';
import 'package:bilalqwatly_test/models/response/photo_response_model.dart';
import 'package:bilalqwatly_test/models/response/interests_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'location_response_model.dart';

part 'profile_response_model.g.dart';

@JsonSerializable()
class ProfileResponseModel {


  @JsonKey(name: 'photos')
  List<PhotoResponseModel>? photos;

  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'gender')
  String? gender;
  @JsonKey(name: 'age')
  int? age;

  @JsonKey(name: 'location')
  LocationResponseModel? location;

  @JsonKey(name: 'basic_info')
  List<BasicInfoResponseModel>? basicInfoList;

  @JsonKey(name: 'interests')
  List<InterestsResponseModel>? interestsList;





  ProfileResponseModel({
    required this.photos,
    required this.name,
    required this.age,
    required this.gender,
    required this.location,
    required this.basicInfoList,
    required this.interestsList,
  });

  factory ProfileResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ProfileResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileResponseModelToJson(this);
}
