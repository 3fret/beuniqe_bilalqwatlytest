// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'basic_info_key_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BasicInfoKeyResponseModel _$BasicInfoKeyResponseModelFromJson(
        Map<String, dynamic> json) =>
    BasicInfoKeyResponseModel(
      name: json['name'] as String?,
    );

Map<String, dynamic> _$BasicInfoKeyResponseModelToJson(
        BasicInfoKeyResponseModel instance) =>
    <String, dynamic>{
      'name': instance.name,
    };
