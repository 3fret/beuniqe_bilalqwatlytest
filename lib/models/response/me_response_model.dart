import 'package:bilalqwatly_test/models/response/profile_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'me_response_model.g.dart';

@JsonSerializable()
class MeResponseModel {

  @JsonKey(name: 'profile')
  ProfileResponseModel? profile;

  MeResponseModel({
    required this.profile,
  });

  factory MeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$MeResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$MeResponseModelToJson(this);
}
