// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'basic_info_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BasicInfoResponseModel _$BasicInfoResponseModelFromJson(
        Map<String, dynamic> json) =>
    BasicInfoResponseModel(
      value: json['value'] as String?,
      key: json['key'] == null
          ? null
          : BasicInfoKeyResponseModel.fromJson(
              json['key'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BasicInfoResponseModelToJson(
        BasicInfoResponseModel instance) =>
    <String, dynamic>{
      'value': instance.value,
      'key': instance.key,
    };
