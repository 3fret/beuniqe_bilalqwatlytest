import 'package:json_annotation/json_annotation.dart';

part 'pagination_response_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class PaginationResponseModel<T> {
  @JsonKey(name: 'TotalSize')
  int? totalSize;

  @JsonKey(name: 'Items')
  T? items;

  PaginationResponseModel({required this.totalSize, required this.items});

  factory PaginationResponseModel.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$PaginationResponseModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) =>
      _$PaginationResponseModelToJson(this, toJsonT);
}
