import 'package:json_annotation/json_annotation.dart';
part 'api_response_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ApiResponseModel<T> {
  @JsonKey(name: 'status')
  String? status;
  @JsonKey(name: 'data')
  T? data;

  ApiResponseModel({required this.status, required this.data});

  factory ApiResponseModel.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ApiResponseModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) =>
      _$ApiResponseModelToJson(this, toJsonT);
}
