import 'package:json_annotation/json_annotation.dart';

//flutter pub run build_runner build
part 'meta_model.g.dart';

@JsonSerializable()
class MetaModel {
  @JsonKey(name: "Status")
  bool? status;

  @JsonKey(name: "Message")
  String? message ;

  @JsonKey(name: "StatusCode")
  int? statusCode;

  @JsonKey(name: "PicturePath")
  String? picturePath;

  MetaModel(
      {required this.status,
      required this.message,
      required this.statusCode,
      required this.picturePath});

  factory MetaModel.fromJson(Map<String, dynamic> json) =>
      _$MetaModelFromJson(json);

  Map<String, dynamic> toJson() => _$MetaModelToJson(this);
}
