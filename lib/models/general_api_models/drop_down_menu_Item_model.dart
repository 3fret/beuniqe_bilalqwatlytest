class DropdownMenuItemModel {
   int? id;
   String? value;
   String? image;

  DropdownMenuItemModel({ this.id,  this.value,this.image});
}
