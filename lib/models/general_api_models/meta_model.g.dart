// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meta_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MetaModel _$MetaModelFromJson(Map<String, dynamic> json) => MetaModel(
      status: json['Status'] as bool?,
      message: json['Message'] as String?,
      statusCode: json['StatusCode'] as int?,
      picturePath: json['PicturePath'] as String?,
    );

Map<String, dynamic> _$MetaModelToJson(MetaModel instance) => <String, dynamic>{
      'Status': instance.status,
      'Message': instance.message,
      'StatusCode': instance.statusCode,
      'PicturePath': instance.picturePath,
    };
