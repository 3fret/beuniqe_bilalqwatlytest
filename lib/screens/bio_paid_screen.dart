import 'package:bilalqwatly_test/base_screen/base_future_builder_page.dart';
import 'package:bilalqwatly_test/list_items/list_item_personal_info.dart';
import 'package:bilalqwatly_test/main.dart';
import 'package:bilalqwatly_test/models/response/me_response_model.dart';
import 'package:bilalqwatly_test/providers/app_provider.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_constants.dart';
import 'package:bilalqwatly_test/utilities/app_images.dart';
import 'package:bilalqwatly_test/utilities/app_localizations.dart';
import 'package:bilalqwatly_test/utilities/app_style_text.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:bilalqwatly_test/widgets/custom_carousel_slider.dart';
import 'package:bilalqwatly_test/widgets/raised_gradient_custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class BioPaidScreen extends BaseFutureBuilderPage {
  const BioPaidScreen({Key? key}) : super(key: key);

  @override
  _BioPaidScreenState createState() => _BioPaidScreenState();
}

class _BioPaidScreenState extends BaseFutureBuilderPageState<BioPaidScreen> {
  MeResponseModel? _meResponseModel;

  @override
  void onInit() {
    super.onInit();

    /// you should init BaseResponseModel
    /// for future builder
    baseResponseModel =
        Provider.of<AppProvider>(context, listen: false).getMeProfile(
      context,
    );
  }

  @override
  void onError() {
    setState(() {
      onInit();
    });
  }

  @override
  Widget onSuccess(response) {
    _meResponseModel = response as MeResponseModel;

    return SingleChildScrollView(
      reverse: false,
      child: Column(
        children: [
          ///Slider
          CustomCarouselSlider(photoList: _meResponseModel!.profile!.photos!),

          ///Size between Slider and name ,age
          SizedBox(
            height: AppSuitableWidgetSize.getSuitableWidgetHeight(20),
          ),

          Container(
            padding: EdgeInsets.symmetric(
                horizontal: AppSuitableWidgetSize.getSuitableWidgetWidth(40)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ///Name and Age
                Text(
                  _meResponseModel!.profile!.name! +
                      ", " +
                      _meResponseModel!.profile!.age.toString(),
                  style: AppStyleText.helveticaNeuMedium16WhiteFFFFFF(),
                ),

                /// Space Between Name and Age and Lorem ipsum
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                ),

                /// Lorem ipsum
                Text(
                  AppLocalizations.of(context)!.translate("lorem"),
                  style: AppStyleText.arialRegular14WhiteFFFFFF(),
                ),

                /// Space Between Lorem ipsum and Basic Info
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(25),
                ),

                ///Basic Info
                Text(
                  AppLocalizations.of(context)!.translate("basic_info"),
                  style: AppStyleText.helveticaNeuMedium16WhiteFFFFFF(),
                ),

                /// Space Between Basic Info
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(25),
                ),

                ///Basic Info
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal:
                          AppSuitableWidgetSize.getSuitableWidgetWidth(15),
                      vertical:
                          AppSuitableWidgetSize.getSuitableWidgetHeight(15)),
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(230),
                  width: appWidthScreen,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /// Name
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text(
                                AppLocalizations.of(context)!.translate("name"),
                                style: AppStyleText
                                    .helveticaNeuLight17WhiteFFFFFF()),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                _meResponseModel!.profile!.name!,
                                style: AppStyleText
                                    .helveticaNeuMedium16WhiteFFFFFF(),
                              )),
                        ],
                      ),
                      SizedBox(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                      ),
                      Divider(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(2),
                        thickness:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(2),
                        color: AppColors.blue0A2175_37,
                      ),
                      SizedBox(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                      ),

                      /// Gender
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text(
                                AppLocalizations.of(context)!
                                    .translate("gender"),
                                style: AppStyleText
                                    .helveticaNeuLight17WhiteFFFFFF()),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                _meResponseModel!.profile!.gender!,
                                style: AppStyleText
                                    .helveticaNeuMedium16WhiteFFFFFF(),
                              )),
                        ],
                      ),
                      SizedBox(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                      ),
                      Divider(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(2),
                        thickness:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(2),
                        color: AppColors.blue0A2175_37,
                      ),
                      SizedBox(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                      ),

                      /// Age
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text(
                                AppLocalizations.of(context)!.translate("age"),
                                style: AppStyleText
                                    .helveticaNeuLight17WhiteFFFFFF()),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                _meResponseModel!.profile!.age!.toString(),
                                style: AppStyleText
                                    .helveticaNeuMedium16WhiteFFFFFF(),
                              )),
                        ],
                      ),
                      SizedBox(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                      ),
                      Divider(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(2),
                        thickness:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(2),
                        color: AppColors.blue0A2175_37,
                      ),
                      SizedBox(
                        height:
                            AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                      ),

                      /// Location
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text(
                                AppLocalizations.of(context)!
                                    .translate("location"),
                                style: AppStyleText
                                    .helveticaNeuLight17WhiteFFFFFF()),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                _meResponseModel!.profile!.location!.address!,
                                maxLines: 1,
                                style: AppStyleText
                                    .helveticaNeuMedium16WhiteFFFFFF(),
                              )),
                        ],
                      ),
                    ],
                  ),
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(17)),
                      color: AppColors.blue495896_60),
                ),

                /// Space Between Personal Info
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(35),
                ),

                ///Basic Info
                Text(
                  AppLocalizations.of(context)!.translate("personal_info"),
                  style: AppStyleText.helveticaNeuMedium16WhiteFFFFFF(),
                ),

                /// Space Between Basic Info and table
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(35),
                ),

                ///Basic Info ListView
                Container(
                  child: ListView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsetsDirectional.only(
                      start: AppSuitableWidgetSize.getSuitableWidgetWidth(15),
                      end: AppSuitableWidgetSize.getSuitableWidgetWidth(15),
                      top: AppSuitableWidgetSize.getSuitableWidgetHeight(20),
                      bottom: AppSuitableWidgetSize.getSuitableWidgetHeight(0),
                    ),
                    scrollDirection: Axis.vertical,
                    physics: const BouncingScrollPhysics(),
                    itemCount: _meResponseModel!.profile!.basicInfoList!.length,
                    itemBuilder: (BuildContext context, int index) {
                      bool isLast = index !=
                          _meResponseModel!.profile!.basicInfoList!.length - 1;
                      return ListItemPersonalInfo(
                          basicInfoResponseModel: _meResponseModel!
                              .profile!.basicInfoList!
                              .elementAt(index),
                          islast: isLast);
                    },
                  ),
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(17)),
                      color: AppColors.blue495896_60),
                ),

                /// Space Between Basic Info ListView and Passions
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(35),
                ),

                ///Passions Text
                Text(
                  AppLocalizations.of(context)!.translate("passions_small"),
                  style: AppStyleText.helveticaNeuMedium16WhiteFFFFFF(),
                ),

                ///Passions GridView
                SizedBox(
                  height: 400,
                  child: GridView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsetsDirectional.only(
                      top: AppSuitableWidgetSize.getSuitableWidgetHeight(30),
                    ),
                    scrollDirection: Axis.horizontal,
                    gridDelegate: SliverStairedGridDelegate(
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 20,
                      tileBottomSpace: 50,
                      pattern: const [
                        StairedGridTile(0.3, 1),
                        StairedGridTile(0.35, 0.7),
                        StairedGridTile(0.25, 1),
                      ],
                    ),
                    physics: const BouncingScrollPhysics(),
                    itemCount: _meResponseModel!.profile!.interestsList!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Stack(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: AppSuitableWidgetSize
                                    .getSuitableWidgetWidth(5)),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: const LinearGradient(
                                colors: <Color>[
                                  AppColors.blue4B6FFF,
                                  AppColors.blue0226B2
                                ],
                              ),
                              image: DecorationImage(
                                  image:
                                      AppImages.getCachedNetworkImageProvider(
                                          AppConstants.baseImageUrl +
                                              _meResponseModel!
                                                  .profile!.interestsList!
                                                  .elementAt(index)
                                                  .image!)),
                            ),
                          ),
                          Positioned(
                            child: Text(
                              _meResponseModel!.profile!.interestsList!
                                  .elementAt(index)
                                  .name!,
                              textAlign: TextAlign.center,
                              style: AppStyleText
                                  .helveticaNeuRegular12WhiteFFFFFF(),
                            ),
                            width: AppSuitableWidgetSize.getSuitableWidgetWidth(
                                80),
                          ),
                        ],
                        alignment: Alignment.center,
                      );
                    },
                  ),
                ),


                /// Report
                RaisedGradientCustomButton(
                  color: AppColors.blue081C71,
                  text: AppLocalizations.of(context)!.translate("report"),
                ),

                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                ),
                /// Unpair
                RaisedGradientCustomButton(
                  color: AppColors.blue2699FB_35,
                  text: AppLocalizations.of(context)!.translate("unpair"),
                ),
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(15),
                ),
                /// Block
                RaisedGradientCustomButton(
                  color: AppColors.grayDEDEDE_35,
                  text: AppLocalizations.of(context)!.translate("block"),
                ),


                /// Space Between Basic Info
                SizedBox(
                  height: AppSuitableWidgetSize.getSuitableWidgetHeight(100),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
