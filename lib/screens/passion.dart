import 'package:bilalqwatly_test/models/response/passion_response_model.dart';
import 'package:bilalqwatly_test/providers/app_provider.dart';
import 'package:bilalqwatly_test/screens/bio_paid_screen.dart';
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/utilities/app_constants.dart';
import 'package:bilalqwatly_test/utilities/app_images.dart';
import 'package:bilalqwatly_test/utilities/app_localizations.dart';
import 'package:bilalqwatly_test/utilities/app_style_text.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:bilalqwatly_test/utilities/utility.dart';
import 'package:bilalqwatly_test/widgets/raised_gradient_button.dart';
import 'package:bilalqwatly_test/base_screen/base_future_builder_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class PassionScreen extends BaseFutureBuilderPage {
  const PassionScreen({Key? key}) : super(key: key);

  @override
  _PassionScreenState createState() => _PassionScreenState();
}

class _PassionScreenState extends BaseFutureBuilderPageState<PassionScreen> {
  PassionResponseModel? _passionResponseModel;

@override
  void onInit() {
    super.onInit();
    /// you should init BaseResponseModel
    /// for future builder
    baseResponseModel =
        Provider.of<AppProvider>(context, listen: false).getPassion(
          context,
        );
  }

  @override
  void onError() {
    setState(() {
      onInit();
    });
  }

  @override
  Widget onSuccess(response) {
    _passionResponseModel = response as PassionResponseModel;

    return Padding(
      padding: EdgeInsets.only(
          top: AppSuitableWidgetSize.getSuitableWidgetHeight(30)),
      child: Column(
        children: [
          /// Toolbar
          Stack(
            children: [
              Align(
                alignment: AlignmentDirectional.topStart,
                child: Padding(
                    padding: EdgeInsetsDirectional.only(
                        start:
                            AppSuitableWidgetSize.getSuitableWidgetWidth(30)),
                    child: SvgPicture.asset(
                      'assets/images/backward_arrow.svg',
                      width: AppSuitableWidgetSize.getSuitableWidgetWidth(16),
                      height: AppSuitableWidgetSize.getSuitableWidgetHeight(16),
                    )),
              ),
              Align(
                alignment: AlignmentDirectional.center,
                child: Text(
                  AppLocalizations.of(context)!.translate('passions'),
                  style: AppStyleText.helveticaNeuBold16WhiteFFFFFF(),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),

          /// Space Between toolbar and what
          SizedBox(
            height: AppSuitableWidgetSize.getSuitableWidgetHeight(70),
          ),

          /// Text What are you into
          Text(
            AppLocalizations.of(context)!.translate('what_are_you_into'),
            style: AppStyleText.helveticaNeuLight32WhiteFFFFFF(),
            textAlign: TextAlign.center,
          ),

          /// Space Between Text What are you into and pick_at_least_5
          SizedBox(
            height: AppSuitableWidgetSize.getSuitableWidgetHeight(30),
          ),

          /// Text pick_at_least_5
          Text(
            AppLocalizations.of(context)!.translate('pick_at_least_5'),
            style: AppStyleText.arialRegular14WhiteFFFFFF(),
            textAlign: TextAlign.center,
          ),

          /// Space Between Text pick_at_least_5 and gridview
          SizedBox(height: AppSuitableWidgetSize.getSuitableWidgetHeight(20)),

          ///gridview
          SizedBox(
              height: AppSuitableWidgetSize.getSuitableWidgetHeight(450),
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: Scrollbar(
                  scrollbarOrientation: ScrollbarOrientation.bottom,
                  isAlwaysShown: true,
                  hoverThickness: 2,
                  thickness: 2,
                  interactive: true,
                  showTrackOnHover: true,
                  child: GridView.builder(
                    shrinkWrap: false,

                    padding: EdgeInsetsDirectional.only(
                      start: AppSuitableWidgetSize.getSuitableWidgetWidth(10),
                      end: AppSuitableWidgetSize.getSuitableWidgetWidth(35),
                      top: AppSuitableWidgetSize.getSuitableWidgetHeight(30),
                      bottom: AppSuitableWidgetSize.getSuitableWidgetHeight(30),
                    ),
                    scrollDirection: Axis.horizontal,
                    gridDelegate: SliverStairedGridDelegate(
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 20,
                      tileBottomSpace: 50,
                      pattern: const [
                        StairedGridTile(0.3, 1),
                        StairedGridTile(0.35, 0.7),
                        StairedGridTile(0.25, 1),
                      ],
                    ),
                    physics: const BouncingScrollPhysics(),
                    itemCount: _passionResponseModel!.interestsList!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        child: Stack(
                          children: [
                            Transform(
                              transform: scaleXYZTransform(
                                  scaleX: _passionResponseModel!.interestsList!
                                          .elementAt(index)
                                          .isSelected!
                                      ? 1.1
                                      : 1,
                                  scaleY: _passionResponseModel!.interestsList!
                                          .elementAt(index)
                                          .isSelected!
                                      ? 1.1
                                      : 1),
                              child: Opacity(
                                opacity: _passionResponseModel!.interestsList!
                                        .elementAt(index)
                                        .isSelected!
                                    ? 1
                                    : 0.27,
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: AppSuitableWidgetSize
                                          .getSuitableWidgetWidth(5)),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    gradient: const LinearGradient(
                                      colors: <Color>[
                                        AppColors.blue4B6FFF,
                                        AppColors.blue0226B2
                                      ],
                                    ),
                                    image: DecorationImage(
                                        image: AppImages
                                            .getCachedNetworkImageProvider(
                                            AppConstants.baseImageUrl +
                                                _passionResponseModel!
                                                    .interestsList!
                                                    .elementAt(index)
                                                    .image!)),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              child: Text(
                                _passionResponseModel!.interestsList!
                                    .elementAt(index)
                                    .name!,
                                textAlign: TextAlign.center,
                                style: AppStyleText
                                    .helveticaNeuRegular12WhiteFFFFFF(),
                              ),
                              width:
                                  AppSuitableWidgetSize.getSuitableWidgetWidth(
                                      80),
                            ),
                          ],
                          alignment: Alignment.center,
                        ),
                        onTap: () {
                          setState(() {
                            _passionResponseModel!.interestsList!
                                    .elementAt(index)
                                    .isSelected =
                                !_passionResponseModel!.interestsList!
                                    .elementAt(index)
                                    .isSelected!;
                          });
                        },
                      );
                    },
                  ),
                ),
              )),

          /// Space Between gridview and Button continue
          SizedBox(
            height: AppSuitableWidgetSize.getSuitableWidgetHeight(20),
          ),

          ///  Button continue
          RaisedGradientButton(
            child: Text(
              AppLocalizations.of(context)!.translate("continue"),
              style: AppStyleText.helveticaNeuBold14WhiteFFFFFF(),
            ),
            gradient: const LinearGradient(
              colors: <Color>[AppColors.blue4B6FFF, AppColors.blue0226B2],
            ),
            onPressed: () {
              if (isPickedFiveInterests()) {
                SchedulerBinding.instance!
                    .addPostFrameCallback((_) async {
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.fade,
                          child:const BioPaidScreen()));
                });

              } else {
                Utility.showToast(context,
                    AppLocalizations.of(context)!.translate("pick_at_least_5"));
              }
            },
          ),
        ],
      ),
    );
  }

  bool isPickedFiveInterests() {
    int count = 0;
    for (var element in _passionResponseModel!.interestsList!) {
      if (element.isSelected!) {
        count++;
      }
    }
    return count >= 5;
  }

  Matrix4 scaleXYZTransform({
    double scaleX = 1.00,
    double scaleY = 1.00,
    double scaleZ = 1.00,
  }) {
    return Matrix4.diagonal3Values(scaleX, scaleY, scaleZ);
  }
}
