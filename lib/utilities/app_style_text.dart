

import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'app_suitable_widget_size.dart';

class AppStyleText{



  ///Arial
  static TextStyle arial32BlackFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(26),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Arial",
        fontWeight: FontWeight.w600);
  }



  static TextStyle arialRegular14WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(14),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Arial",
        fontWeight: FontWeight.w400);
  }


  ///Helvetica-Neu-Bold
  static TextStyle helveticaNeuBold16WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(16),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w700);
  }

  static TextStyle helveticaNeuBold13WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(13),
        color: AppColors.whiteFFFFFF_60,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w700);
  }

  static TextStyle helveticaNeuBold14WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(14),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w700);
  }
  ///Helvetica-Neu-Regular
  static TextStyle helveticaNeuRegular12WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(12),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w400);
  }

  ///Helvetica-Neu-Light
  static TextStyle helveticaNeuLight32WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(32),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w300);
  }
  static TextStyle helveticaNeuLight17WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(17),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w200);
  }


  ///Helvetica-Neu-Medium
  static TextStyle helveticaNeuMedium16WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(16),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w500);
  }

  ///Helvetica-Neu-Medium
  static TextStyle helveticaNeuMedium17WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(17),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Helvetica-Neu",
        fontWeight: FontWeight.w500);
  }

  ///Poppins

  static TextStyle poppinsRegular16GrayCDCDCD() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(16),
        color: AppColors.grayCDCDCD,
        fontFamily: "Poppins",
        fontWeight: FontWeight.w200);
  }
  static TextStyle poppinsRegular13GrayCDCDCD() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(13),
        color: AppColors.grayCDCDCD,
        fontFamily: "Poppins",
        fontWeight: FontWeight.w200);
  }

  static TextStyle poppinsRegular18GrayCDCDCD() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(18),
        color: AppColors.grayCDCDCD,
        fontFamily: "Poppins",
        fontWeight: FontWeight.w500);
  }

  static TextStyle poppinsRegular16WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(16),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Poppins",
        fontWeight: FontWeight.w200);
  }

  static TextStyle poppins13RedFF0000() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(13),
        color: AppColors.redFF0000,
        fontFamily: "Poppins",
        fontWeight: FontWeight.w200);
  }



  ///Acme
  static TextStyle acme32WhiteFFFFFF() {
    return TextStyle(
        fontSize: AppSuitableWidgetSize.getSuitableWidgetWidth(32),
        color: AppColors.whiteFFFFFF,
        fontFamily: "Acme",
        fontWeight: FontWeight.w400);
  }



}