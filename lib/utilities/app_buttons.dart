import 'package:bilalqwatly_test/utilities/app_style_text.dart';
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';
import 'package:bilalqwatly_test/widgets/custom_button.dart';
import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppButtons {
  static CustomButton greenButton(
      double width,
      VoidCallback onPressed, String text, BuildContext context) {
    return CustomButton(
      onPressed: () {
        onPressed.call();
      },
      text: text,
      width: width,
      height: AppSuitableWidgetSize.getSuitableWidgetHeight(50),
      borderColor: AppColors.green0C7187_67,
      borderWidth: AppSuitableWidgetSize.getSuitableWidgetHeight(0),
      color: AppColors.green0C7187_67,
      radius: AppSuitableWidgetSize.getSuitableWidgetHeight(20),
      textStyle: AppStyleText.poppinsRegular18GrayCDCDCD(),
    );
  }

  static CustomButton blueButton(
      double width,
      VoidCallback onPressed, String text, BuildContext context) {
    return CustomButton(
      onPressed: () {
        onPressed.call();
      },
      text: text,
      width: width,
      height: AppSuitableWidgetSize.getSuitableWidgetHeight(50),
      borderColor: AppColors.green0C7187_67,
      borderWidth: AppSuitableWidgetSize.getSuitableWidgetHeight(0),
      color: AppColors.green0C7187_67,
      radius: AppSuitableWidgetSize.getSuitableWidgetHeight(20),
      textStyle: AppStyleText.poppinsRegular18GrayCDCDCD(),
    );
  }




}
