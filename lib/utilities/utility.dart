import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart'
    '';

import 'app_colors.dart';
import 'app_suitable_widget_size.dart';

class Utility {
  static showToast(BuildContext context ,String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: AppColors.gray585858,
        textColor: AppColors.whiteFFFFFF,
        fontSize: AppSuitableWidgetSize.getSuitableWidgetHeight(14));
  }
}
