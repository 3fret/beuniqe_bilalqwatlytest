import 'package:bilalqwatly_test/main.dart';
import 'package:flutter/cupertino.dart';

class AppSuitableWidgetSize {



  static double getSuitableWidgetHeight(
      double targetWidgetHeight) {

    // It's the AdobeXD Device Height
     double targetHeightSize = 926;
    //  var physicalPixelHeight = MediaQuery.of(context).size.height *
    //    MediaQuery.of(context).devicePixelRatio;
    //print("devicePixelRatio: " +
    //   MediaQuery.of(context).size.aspectRatio.toString());
    // print("physicalPixelHeight: " + physicalPixelHeight.toString());

    if (appAspectRatio > 0.463) {
      targetHeightSize = 736;
      return (appHeightScreen* targetWidgetHeight * 0.85) /
          targetHeightSize;
    } else {
      targetHeightSize = 896;

      return (appHeightScreen * targetWidgetHeight) /
          targetHeightSize; // It's the AdobeXD Device Width

    }
  }

  static double getSuitableWidgetWidth(
      double targetWidgetWidth) {

    // It's the AdobeXD Device Width
     double targetWidthSize = 428;

    if (appAspectRatio> 0.463) {
      targetWidthSize = 414;
    } else {
      targetWidthSize = 428;
    }
    return (appWidthScreen* targetWidgetWidth) /
        targetWidthSize;
  }


}

final appWidgetSizer = AppSuitableWidgetSize();
