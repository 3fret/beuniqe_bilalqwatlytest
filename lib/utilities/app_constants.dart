
class AppConstants {



  //android configuration
  static const String errorApiValue = 'error';

  //android configuration
  static const String packageName = 'com.bilalqwatly.test';

  //ios configuration
  static const String bundleId = 'com.bilalqwatly.test';


  //fonts
  static const String arabicFont = 'Cairo';
  static const String englishFont = 'Cairo';




  static const String cairoRegular = 'Cairo-Regular';
  static const String cairoSemiBold = 'Cairo-SemiBold';
  static const String cairoBold = 'Cairo-Bold';


  static const String baseImageUrl = 'https://api.zipconnect.app/img/interests/';

  //Language
  static const String arabicConst = 'ar';
  static const String englishConst = 'en';



  //Pick Image
  static const double maxWidthConst = 100;
  static const double maxHeightConst = 100;
  static const int qualityConst = 75;


  static const int pageSizeConst = 12;



}
