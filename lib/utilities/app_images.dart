import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'app_colors.dart';
import 'app_constants.dart';

class AppImages {
  static const String assetsImage = 'assetsImage';
  static const String networkImage = 'networkImage';
  static const String fileImage = 'fileImage';

  static Widget getCircleImage(
      String path, double radius, BuildContext context) {
    return CircleAvatar(
        radius: radius,
        backgroundColor: AppColors.whiteFFFFFF,
        backgroundImage:
            CachedNetworkImageProvider(AppConstants.baseImageUrl + path));
  }

  static ImageProvider getCachedNetworkImageProvider(String path) {
    return CachedNetworkImageProvider( path );
  }


  static Widget getNetworkImage(String path, BuildContext context) {
    return CachedNetworkImage(
      imageUrl: AppConstants.baseImageUrl + path,
    );
  }
}
