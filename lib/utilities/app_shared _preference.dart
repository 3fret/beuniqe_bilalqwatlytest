
import '../main.dart';

class AppSharedPreference {
  //shared Preference values
  static const String languagePrefs = 'languageCode';
  static const String tokenPrefs = 'Token';
  static const String isLoggedInPrefs = 'isLoggedInPrefs';


  static void setIsLoggedInPrefs(bool isLoggedIn) {
    prefs.setBool(isLoggedInPrefs, isLoggedIn);
  }

  static bool? getIsLoggedInPrefs() {
    return prefs.getBool(isLoggedInPrefs);
  }

  static void setLanguagePrefs(String lan) async {
    prefs.setString(languagePrefs, lan);
  }
  static String? getLanguagePrefs() {
    return prefs.getString(languagePrefs);
  }


  static void setTokenPrefs(String token) {
    prefs.setString(tokenPrefs, token);
  }

  static String? getTokenPrefs()  {
    return prefs.getString(tokenPrefs);
  }


  // Object example
  // static void setSubscriberProfileModelPrefs(
  //     SubscriberProfileModel subscriberProfileModel) {
  //   prefs.setString(subscriberProfileModelPrefs,
  //       jsonEncode(subscriberProfileModel.toJson()));
  // }
  //
  // static SubscriberProfileModel getSubscriberProfileModelPrefs() {
  //   Map<String, dynamic> json =
  //   jsonDecode(prefs.getString(subscriberProfileModelPrefs) ?? '');
  //   return SubscriberProfileModel.fromJson(json);
  // }

  // ArrayList example
  // static void setCountryList(List<CountryModel> countryList) {
  //   List<String> strList =
  //   countryList.map((i) => jsonEncode(i.toJson())).toList();
  //   prefs.setStringList(countryListPrefs, strList);
  // }
  //
  // static List<CountryModel> getCountryList() {
  //   List<String>? savedStrList = prefs.getStringList(countryListPrefs);
  //
  //   return savedStrList!
  //       .map((i) => CountryModel.fromJson(jsonDecode(i)))
  //       .toList();
  // }
}
