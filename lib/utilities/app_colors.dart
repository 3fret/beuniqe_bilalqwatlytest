import 'package:flutter/material.dart';

class AppColors {
  static const whiteFFFFFF = Color(0xFFFFFFFF);
  static const whiteFFFFFF_10 = Color(0x1AFFFFFF);
  static const whiteFFFFFF_60 = Color(0x99FFFFFF);
  static const black000000 = Color(0xFF000000);
  static const redFF0000 = Color(0xFFFF0000);
  static const blue42C1F7 = Color(0xFF42C1F7);
  static const gray585858 = Color(0xFF585858);
  static const green33F100 = Color(0xFF33F100);
  static const blue7DB6FC = Color(0xFF7DB6FC);
  static const yellowFFBE00 = Color(0xFFFFBE00);
  static const purpleA071F2 = Color(0xFFA071F2);
  static const blue2395F2 = Color(0xFF2395F2);
  static const black070014 = Color(0xFF070014);
  static const black070014_80 = Color(0xCC070014);
  static const grayCDCDCD = Color(0xFFCDCDCD);
  static const grayCDCDCD_67 = Color(0xABCCCCCC);
  static const green0C7187_67 = Color(0xAB0C7187);
  static const blue4B6FFF = Color(0xFF4B6FFF);
  static const blue0226B2 = Color(0xFF0226B2);
  static const gray707070 = Color(0xFF707070);
  static const blue495896_60 = Color(0x994A5996);
  static const blue0A2175_37 = Color(0x5E0B2275);
  static const blue081C71 = Color(0xFF081C71);
  static const blue2699FB_35 = Color(0x592597FA);
  static const grayDEDEDE_35 = Color(0x59DEDEDE);


  // //default map color for all colors
  // static const Map<int, Color> defaultMapColor =
  // {
  //   50:Color.fromRGBO(0,0,0, .1),
  //   100:Color.fromRGBO(0,0,0, .2),
  //   200:Color.fromRGBO(0,0,0, .3),
  //   300:Color.fromRGBO(0,0,0, .4),
  //   400:Color.fromRGBO(35,0,0, .5),
  //   500:Color.fromRGBO(0,0,0, .6),
  //   600:Color.fromRGBO(0,0,0, .7),
  //   700:Color.fromRGBO(0,0,0, .8),
  //   800:Color.fromRGBO(0,0,0, .9),
  //   900:Color.fromRGBO(0,0,0, 1),
  // };
  //
  // static const MaterialColor greenMaterialAppColor =
  // MaterialColor(0xFF2395F2, defaultMapColor);
  //
  // static const MaterialColor lightBlueMaterialAppColor =
  // MaterialColor(0xFF7DB6FC, defaultMapColor);




}
