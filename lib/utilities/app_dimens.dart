
import 'package:bilalqwatly_test/utilities/app_suitable_widget_size.dart';

class AppDimens{


  static double xxxSmallWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(2);
  static double xxxSmallHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(2);

  static double xxSmallWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(4);
  static double xxSmallHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(4);

  static double xSmallWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(6);
  static double xSmallHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(6);

  static double smallWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(8);
  static double smallHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(8);

  static double mediumWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(10);
  static double mediumHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(10);

  static double largeWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(12);
  static double largeHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(12);

  static double xLargeWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(14);
  static double xLargeHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(14);

  static double xXLargeWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(16);
  static double xXLargeHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(16);

  static double xxXLargeWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(18);
  static double xxxLargeHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(18);

  static double xxxXLargeWidth = AppSuitableWidgetSize.getSuitableWidgetHeight(20);
  static double xxxXLargeHeight = AppSuitableWidgetSize.getSuitableWidgetHeight(20);


}