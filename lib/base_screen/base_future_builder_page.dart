
import 'package:bilalqwatly_test/utilities/app_colors.dart';
import 'package:bilalqwatly_test/widgets/error_page.dart';
import 'package:bilalqwatly_test/widgets/loading_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io' show Platform;

import '../main.dart';

class BaseFutureBuilderPage extends StatefulWidget {
  const BaseFutureBuilderPage({Key? key}) : super(key: key);

  @override
  BaseFutureBuilderPageState createState() => BaseFutureBuilderPageState();
}

class BaseFutureBuilderPageState<T extends BaseFutureBuilderPage>
    extends State<T> {
  late var baseResponseModel;

  void onInit() {}

  void onError() {}

  Widget onLoading() {

    return const LoadingPage();
  }

  Widget onSuccess(response) {
    return const Text('Base onSuccess');
  }

  @override
  void initState() {
    onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(

      backgroundColor: AppColors.black070014,
      appBar: AppBar(
        backwardsCompatibility: Platform.isAndroid ? false : true,
        //for ios status bar color

        backgroundColor: AppColors.black070014,
        //for android status bar color
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: AppColors.black070014,

        ),
        toolbarHeight: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: appWidthScreen,
          height: appHeightScreen,
          decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.png"),
                fit: BoxFit.cover,
              )),
          child: FutureBuilder(
            future: baseResponseModel,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {

                return onLoading();
              } else {
                if (snapshot.hasError) {
                  return ErrorPage(onPressed: () {
                    onError();
                  });
                } else {
                  return onSuccess(snapshot.data!);
                }
              }
            },
          ),
        ),
      ),
    );
  }
}
